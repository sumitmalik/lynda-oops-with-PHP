<?php

require 'class.Address.inc';

echo '<h2>Instantiating Address</h2>';

$address = new Address;

echo '<h2>Empty Address</h2>';
echo '<tt><pre>' . var_export($address, TRUE) . '</pre></tt>';

echo '<h2>Setting Properties...</h2>';
$address->street_address_1 = '4, Gali no. 7';
$address->city_name = 'Amritsar';
$address->subdivition_name = 'Amritsar';
$address->postal_code = '143001';
$address->country_name = 'India';

echo '<tt><pre>' . var_export($address, TRUE) . '</pre></tt>';

echo '<h2>Displaying Address...</h2>';
echo $address->display();

echo '<h2>Checking protected properties...</h2>';
echo 'Address ID: ' . $this->_address_id;