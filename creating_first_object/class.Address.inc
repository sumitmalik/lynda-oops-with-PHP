<?php

/**
 * Physical Address
 */
class Address
{
    // Street Address
    public $street_address_1;
    public $street_address_2;

    // Name of the city
    public $city_name;

    // Name of the subdivision
    public $subdivition_name;

    // Postal Code
    public $postal_code;

    // Name of the country
    public $country_name;

    // Primary key of the address
    protected $_address_id;

    // When was the record created and last updated
    protected $_time_created;
    protected $_time_updated;

    /**
     * Display an address in HTML format.
     * @return string
     */
    public function display()
    {
        $output = '';

        // Street Address
        $output .= $this->street_address_1;
        if ($this->street_address_2) {
            $output .= '<br />' . $this->street_address_2;
        }

        // City, Subdivision and Postal
        $output .= '<br />' . $this->city_name . ', ' . $this->subdivition_name . ' ' . $this->postal_code;

        // Country
        $output .= '<br />' . $this->country_name;
        
        return $output;
    }
}